﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Travel
{
    class Trip
    {
        public string Destination { get; set; }
        public string Name { get; set; }
        public string Passport { get; set; }
        public string DateDepature { get; set; }

        public string DateReturn { get; set; }

        public Trip(string destination, string name, string passport, string dateDepature, string datereturn)
        {
            Destination = destination;
            Name = name;
            Passport = passport;
            DateDepature = dateDepature;
            DateReturn = datereturn;
        }

        public override string ToString()
        {
            return string.Format("{0} {1} {2} {3} {4} {5}", Destination, Name, Passport, DateDepature, DateReturn);
        }

        public string ToDataString()
        {
            return string.Format("{0};{1};{2};{3};{4}", Destination, Name, Passport, DateDepature, DateReturn);
        }
    }
}
