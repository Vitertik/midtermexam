﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Travel
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public string fileName = @"..\..\trips.txt";

        List<Trip> trips = new List<Trip>();
        public MainWindow()
        {
            InitializeComponent();
            LoadFile();
        }

        private void LoadFile()
        {
            using (StreamReader sr = new StreamReader(fileName))
            {
                string line = sr.ReadLine();
                while (line != null)
                {
                    string[] myData = line.Split(';');
                    Trip trip = new Trip(myData[0], myData[1], myData[2], myData[3], myData[4]);
                    trips.Add(trip);
                    line = sr.ReadLine();
                }
                lvList.ItemsSource = trips;
            }
        }


            /*
                        if (File.Exists(fileName))
                        {
                            string[] tripInfo = File.ReadAllLines(fileName);

                            foreach (string tripLine in tripInfo)
                            {
                                string[] tripData = tripLine.Split(';');
                                Trip t = new Trip(tripData[0], tripData[1], tripData[2], tripData[3], tripData[4]);
                                trips.Add(t);
                            }
                        }

                        lvList.ItemsSource = trips;
                    }*/

            private void lvList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            btnDelete.IsEnabled = true;
            btnUpdate.IsEnabled = true;

            var selectedTrip = lvList.SelectedItem;
 
            if (selectedTrip is Trip)
            {
                Trip trip = (Trip)lvList.SelectedItem;
                txtDestination.Text = trip.Destination;
                txtName.Text = trip.Name;
                txtPassport.Text = trip.Passport;
                dpDepature.SelectedDate = DateTime.Parse(trip.DateDepature);
                dpReturn.SelectedDate = DateTime.Parse(trip.DateReturn);

            }
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            if ((txtDestination.Text == ""))
            {
                MessageBox.Show("Input string error.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            DateTime? selectedDepature = dpDepature.SelectedDate;
            DateTime? selectedReturn = dpReturn.SelectedDate;
            if (selectedDepature == null || selectedReturn == null)
            {
                MessageBox.Show("Choose a date for the TODO", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            Trip trip = new Trip(txtDestination.Text, txtName.Text, txtPassport.Text, selectedDepature.ToString(), selectedReturn.ToString());
            trips.Add(trip);
            ResetValue();
        }

        private void ResetValue()
        {
            lvList.Items.Refresh();
            txtDestination.Clear();
            txtName.Clear();
            txtPassport.Clear();
            dpDepature.Text = "";
            dpReturn.Text = "";
            lvList.SelectedIndex = -1;
            btnDelete.IsEnabled = false;
            btnUpdate.IsEnabled = false;
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            if (lvList.SelectedIndex == -1)
            {
                MessageBox.Show("You need to select one trip", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            Trip tripToBeDeleted = (Trip)lvList.SelectedItem;
            if (tripToBeDeleted != null)
            {
                MessageBoxResult result = MessageBox.Show("Are you sure to delete the trip?", "CONFIRMATION", MessageBoxButton.YesNo, MessageBoxImage.Warning);
                if (result == MessageBoxResult.Yes)
                {
                    trips.Remove(tripToBeDeleted);
                    ResetValue();
                }
            }

        }

        private void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            if (lvList.SelectedIndex == -1)
            {
                MessageBox.Show("You need to select one trip", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (txtDestination.Text == "")
            {
                MessageBox.Show("Input string error.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            Trip tasktoBeUpdated = (Trip)lvList.SelectedItem;
            tasktoBeUpdated.Destination = txtDestination.Text;
            tasktoBeUpdated.Name = txtName.Text;
            tasktoBeUpdated.Passport = txtPassport.Text;
            tasktoBeUpdated.DateDepature = dpDepature.Text;
            tasktoBeUpdated.DateReturn = dpReturn.Text;

            ResetValue();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            SaveFile();
        }

        private void SaveFile()
        {
            using (StreamWriter writer = new StreamWriter(fileName))
            {
                foreach (Trip trip in trips)
                {
                    writer.WriteLine(trip.ToDataString());
                }
            }
        }

        private void btnSaveSelected_Click(object sender, RoutedEventArgs e)
        {
 
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "my extension (*.trips)|*.trips";
            saveFileDialog.DefaultExt = "trips";
            saveFileDialog.AddExtension = true;
            saveFileDialog.Title = "Save the all trips in a File";
            if (saveFileDialog.ShowDialog() == true)
            {
                string allData = "";
                foreach (Trip t in trips)
                {
                    allData += t.ToString() + "\n";
                }
                File.WriteAllText(saveFileDialog.FileName, allData);
            }
            else
            {
                MessageBox.Show("File error", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
